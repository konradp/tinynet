#!/bin/bash
# This sets up a server/listener with nmap's ncat tool
# it then sends a http response to any clients that connect to it
# Test with: telnet localhost 5000

# A simple HTTP message (both headers and body)
HTTP_MSG=$'HTTP/1.1 200 OK
Header1: lol
Header2: bla

<html>
yolo
</html>
'

ncat \
    --listen 5000 \
    --keep-open \
    --verbose \
    --sh-exec "echo '$HTTP_MSG'"

