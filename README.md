# Tinynet

A small wrapper class for cURL. 
Provides only minimal functionality: downloads a HTTP response into a string. 

To test, run the mini server/listener:
```
    ./server.sh
```
Note: To use the above, you need the `ncat` tool, which is part of `nmap`.
Then, see the `examples/README.md` file for how to compile and run the examples.

TODO: Throw exception if error
